**Start Docker Server**

**Composer**

touch composer.json
composer self-update
composer install

**Laravel MANAGER APP:**

docker-compose up -d
docker-compose up -d --build --remove-orphans
docker-compose up -d --force-recreate

docker-compose exec webserver bash

docker-compose pull [service_name]

docker ps
docker image ls
docker rmi [IMAGEID]

docker run -it [image_name] sh


// Change App Name
// Laravel 6 do not have this command
// Install composer require andrey-helldar/laravel-app --dev

php artisan app:name App

php artisan migrate:fresh --seed
php artisan l5-swagger:generate
php artisan make:migration create_students_table
php artisan make:controller API/StudentController --resource

php artisan migrate
php artisan migrate:fresh
php artisan migrate:fresh --seed
php artisan migrate:refresh --seed

php artisan make:seeder AuthorsTableSeeder
php artisan migrate:fresh
php artisan make:factory AuthorFactory
php artisan db:seed

// Delete migration then
php artisan migrate:rollback
composer dump-autoload

// Faker
https://github.com/fzaninotto/Faker

// Install Tinker
composer require laravel/tinker
php artisan tinker
>>> Author::first()
>>> Author::all()
>>> Author::first()->name()

//Controller
php artisan make:controller RegularController

//Resource Controller
php artisan make:controller ResourceController -r
php artisan make:controller AuthorController -r --api

//Routes
php artisan route:list

// Resource
php artisan make:resource AuthorResource


**Angular:**

cd webapp

ng serve



**Turn off:**

CTRL + c

docker-compose down

**Configuration**

php artisan app:name Betel

Open app/config/app.php:

changes in .env

timezone = "America/Puerto_Rico"
// https://www.php.net/manual/en/timezones.america.php

APP_ENV = "local" // production

APP_DEBUG = "true"

locale = "en" 
// resources/language/en (create es)
// https://www.github.com/caouecs/Laravel-lang

Open app/config/database.php:

DB_CONNECTION = 'mysql'

Connection:

    'mysql' => [
        'driver' => 'mysql',
        'host' => env('DB_HOST', '127.0.0.1'),
        'port' => env('DB_PORT', '3306'),
        'database' => env('DB_DATABASE', 'forge'),
        'username' => env('DB_USERNAME', 'forge'),
        'password' => env('DB_PASSWORD', ''),
        'unix_socket' => env('DB_SOCKET', ''),
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => 'lid_',
        'strict' => true,
        'engine' => null,
    ]
